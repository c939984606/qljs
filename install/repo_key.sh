#/bin/bash
download_url=https://gitee.com/c939984606/qljs/raw/master/dependence
if [ ! -d "keytmp" ];then
  mkdir keytmp
  cd keytmp
else
  rm -rf ./keytmp/*
  cd keytmp
fi
wget -O id_rsa_own $download_url/id_rsa_own 
wget -O config $download_url/config
if [ ! -d "/root/.ssh/" ];then
  cd /root
  mkdir .ssh
  cd -
else
  echo "文件夹存在继续运行"
fi
cp -rf id_rsa_own /root/.ssh/id_rsa_own
cp -rf config /root/.ssh/config
chmod 600 /root/.ssh/id_rsa_own
rm -rf ../keytmp
ssh -T git@gitee.com

