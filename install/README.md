### 使用说明

##### 以下操作都在容器的终端中运行，如何进入容器，请执行如下命令：

```
docker exec -it 容器名字 /bin/bash
```

- jd_module_install.sh --> 各种脚本依赖module环境安装

```
wget -O jd_module_install.sh https://gitee.com/c939984606/qljs/raw/master/install/jd_module_install.sh && bash jd_module_install.sh
```
- 青龙面板互助码 code.sh：
```
wget -O jd_code_install.sh https://gitee.com/c939984606/qljs/raw/master/install/jd_code_install.sh && bash jd_code_install.sh
```
    然后添加一个定时任务：获取互助码 0 2 * * * task code.sh