#!/usr/bin/env bash

downloadurl="https://gitee.com/c939984606/qljs/raw/master/dependence"

if [ ! -d "/jd/backup_sh/" ];then
  cd /jd
  mkdir backup_sh
else
  cd /jd
fi
apk add diffutils
nowtime=$(date "+%Y-%m-%d-%H%M%S")
mv jup.sh ./backup_sh/jup.sh_$nowtime
mv jtask.sh ./backup_sh/jtask.sh_$nowtime
mv jshare.sh ./backup_sh/jshare.sh_$nowtime
wget -O jup.sh $downloadurl/jup.sh
wget -O jtask.sh $downloadurl/jtask.sh
wget -O jshare.sh $downloadurl/jshare.sh
chmod +x j*.sh
echo "替换 jup.sh  jtask.sh 完成"
echo "备份文件在 /jd/backup_sh 文件夹下"