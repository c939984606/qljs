#!/usr/bin/env bash

mkdir codes_log_tmp
cd codes_log_tmp
echo -e $log_path > pathtmp.log
if [[ ${Auto_code} == true ]] && [[ "$codename_help" != "" ]]; then
for name in $codename_help; do
	echo $name
	log_name=$(echo -e "$(grep "log" pathtmp.log | awk -F "/" '{print $4}' | head -1 | xargs)")
    echo $log_name
	if [[ "$log_name" == "$name" ]]; then
	code_tmp=$(echo -e $(grep "好友互助码" $log_path | awk -F "\】" '{print $2}' | head -100 | xargs))
	echo $code_temp
		num=1
		for code in $code_tmp; do
			if ((num%5==0)); then
				echo -e "$code<br>" >> code.log
				numn=`expr $num - 4`
				echo -e "<br>↑↑↑↑↑以上为第$numn到第$num个CK的互助码↑↑↑↑↑<br><br>" >> code.log
			else
			    echo -e "$code&\c" >> code.log
            fi
            num=$(($num+1))
		done
		cat code.log
	notify "$name 的互助码组合" "$(cat code.log)"
	rm -rf code.log
    fi
done
fi
cd ..
rm -rf codes_log_tmp